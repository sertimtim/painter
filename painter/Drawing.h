#ifndef DRAWING_H
#define DRAWING_H

#include <QWidget>

class Drawing : public QWidget
{
public:
    explicit Drawing(QWidget *parent = nullptr);
    QPixmap& createPixmap(const QString& typeN);
    QPixmap& getFocusPixmap(const QString& typeN);
    void deletePixmap(const QString& typeN);

protected:
    void paintEvent(QPaintEvent* pe);

private:
    std::map<QString, QPixmap> map_pix;
};

#endif // DRAWING_H
