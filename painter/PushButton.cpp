#include "PushButton.h"
#include <QWidget>

PushButton::PushButton( const QString& str, QWidget* parent)
    : QPushButton(parent)
    , p( false )
{
    setCheckable(true);
    setText(str);
    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Maximum);
}

void PushButton::focusInEvent(QFocusEvent* pe)
{
    QPushButton::focusInEvent(pe);
    //setChecked(true);

    p = true;

    //emit focusChanged();
}

void PushButton::focusOutEvent(QFocusEvent* pe)
{
    QPushButton::focusOutEvent(pe);

    p = false;
    //setChecked(false);

    //emit focusChanged();
}
