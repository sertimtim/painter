#ifndef PUSHBUTTON_H
#define PUSHBUTTON_H

#include <QPushButton>

class PushButton : public QPushButton
{
public:
    PushButton(const QString& str, QWidget* parent = nullptr);

    bool get_p() const { return p; }

protected:
    void focusInEvent(QFocusEvent* pe) override;
    void focusOutEvent(QFocusEvent* pe) override;

private:
    bool p;
};

#endif // PUSHBUTTON_H
