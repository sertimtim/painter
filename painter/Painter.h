#ifndef PAINTER_H
#define PAINTER_H

#include <QMainWindow>
#include "WorkSpace.h"
#include "MenuBar.h"
#include "PushButton.h"
#include <memory>
#include "Figure.h"

class Painter : public QMainWindow
{
public:
    Painter(QWidget *parent = nullptr);
    ~Painter();

public slots:
    void setPlaceholder();
    void drawFigure();
    void focusFigure();
    void deleteFigure();

    void saveImage();

private:
    WorkSpace* ws;
    MenuBar* mb;
    std::list<PushButton*> lst_pb;
    std::map<QString, size_t> map_count;
    std::map<QString, std::vector<int>> map_rect;

    void x_addItemInList(PushButton* pb);

    std::map<QString, std::shared_ptr<Figure>> figure;
};

#endif // PAINTER_H
