#include "MenuBar.h"
#include <QMenu>
#include <QToolBar>

MenuBar::MenuBar(QWidget *parent) : QWidget(parent)
{
    m_file = new QMenu("&File");
    a_save = new QAction("Save...");
    a_save->setShortcut(QKeySequence("CTRL+S"));
    a_save->setIcon(QPixmap("../Doc/Icon/save.png"));
    a_save->setToolTip("Save current Image");

    a_exit = new QAction("Exit");

    m_file->addAction(a_save);
    m_file->addSeparator();
    m_file->addAction(a_exit);
}
