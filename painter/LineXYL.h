#ifndef LINEXYL_H
#define LINEXYL_H

#include <QWidget>
class QLineEdit;
class QComboBox;
class QPushButton;

class LineXYL : public QWidget
{
public:
    LineXYL(QWidget *parent = nullptr);

    QLineEdit* leX() const { return le_x; }
    QLineEdit* leY() const { return le_y; }
    QLineEdit* leL() const { return le_l; }
    QPushButton* pbDraw() const { return pb_draw; }
    QComboBox* choiceFigure() const { return cb_choiceFigure; }

private:
    QLineEdit* le_x;
    QLineEdit* le_y;
    QLineEdit* le_l;
    QPushButton* pb_draw;
    QComboBox* cb_choiceFigure;

};

#endif // LINEXYL_H
