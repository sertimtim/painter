#ifndef OBJECTLIST_H
#define OBJECTLIST_H

#include <QWidget>
class QPushButton;
class QListWidget;

class ObjectList : public QWidget
{
public:
    ObjectList(QWidget *parent = nullptr);

    const QPushButton* pbDelete() const { return pb_delete; }    
    QListWidget* lstWgt() const {return lst_wgt;}

private:
    QPushButton* pb_delete;
    QListWidget* lst_wgt;
};

#endif // OBJECTLIST_H
