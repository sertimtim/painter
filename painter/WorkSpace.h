#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <QWidget>
#include "LineXYL.h"
#include "Drawing.h"
#include "ObjectList.h"

class WorkSpace : public QWidget
{ 
public:
    explicit WorkSpace(QWidget *parent = nullptr);

    LineXYL* lXYL() const { return m_lxyl; }
    Drawing* draw() const { return m_drawing; }
    ObjectList* objList() const { return m_obj_list; }

private:
    LineXYL* m_lxyl;
    Drawing* m_drawing;
    ObjectList* m_obj_list;
};

#endif // WORKSPACE_H
