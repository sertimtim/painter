#include "Drawing.h"
#include <QPainter>

Drawing::Drawing(QWidget *parent) : QWidget(parent)
{
    map_pix.emplace("A", QPixmap(500, 500));
    auto it = map_pix.find("A");
    it->second.fill(QColor(Qt::white));
}

void Drawing::paintEvent(QPaintEvent* pe)
{
    Q_UNUSED(pe);
    for(auto it=map_pix.begin(); it!=map_pix.end(); it++)
    {
        QPainter paint(this);
        paint.drawPixmap(0, 0, it->second.width(), it->second.height(), it->second);
    }
}

QPixmap& Drawing::createPixmap(const QString& typeN)
{
    map_pix.emplace(typeN, QPixmap(500, 500));
    auto it = map_pix.find(typeN);
    it->second.fill(QColor(0, 0, 0, 0));
    return it->second;
}

QPixmap& Drawing::getFocusPixmap(const QString& typeN)
{
    auto it = map_pix.find(typeN);
    return it->second;
}

void Drawing::deletePixmap(const QString& typeN)
{
    auto it = map_pix.find(typeN);
    map_pix.erase(it);
}

