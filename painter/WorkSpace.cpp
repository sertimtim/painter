#include "WorkSpace.h"
#include <QLayout>
#include <QSplitter>

WorkSpace::WorkSpace(QWidget *parent) : QWidget(parent)
{
    m_lxyl = new LineXYL;

    QSplitter* spl = new QSplitter(Qt::Vertical);
    m_drawing = new Drawing;
    spl->addWidget(m_drawing);

    QVBoxLayout* lo_draw = new QVBoxLayout;
    lo_draw->addWidget(m_lxyl);

    lo_draw->addWidget(spl);

    m_obj_list = new ObjectList;
    QHBoxLayout* lo_all = new QHBoxLayout;
    lo_all->addLayout(lo_draw, 4);
    lo_all->addWidget(m_obj_list, 2);

    setLayout(lo_all);
}
