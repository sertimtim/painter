#include "Painter.h"
#include "Circle.h"
#include "Square.h"
#include "Triangle.h"
#include <QLayout>
#include <QComboBox>
#include <QLineEdit>
#include <QMessageBox>
#include <QListWidget>
#include <QMenuBar>
#include <QFileDialog>

Painter::Painter(QWidget *parent)
    : QMainWindow(parent)
{    
    figure.emplace("Circle", new Circle);
    figure.emplace("Triangle", new Triangle);
    figure.emplace("Square", new Square);

    ws = new WorkSpace;

    //QHBoxLayout* lo_ws = new QHBoxLayout;
    //lo_ws->addWidget(ws);
    setCentralWidget(ws);

    mb = new MenuBar;
    menuBar()->addMenu(mb->mFile());

    setPlaceholder();

    //setLayout(lo_ws);

    setFixedSize(800, 640);

    connect(ws->lXYL()->choiceFigure(), &QComboBox::currentTextChanged, this, &Painter::setPlaceholder);
    connect(ws->lXYL()->pbDraw(), &QPushButton::clicked, this, &Painter::drawFigure);
    connect(ws->objList()->pbDelete(), &QPushButton::clicked, this, &Painter::deleteFigure);

    connect(mb->aExit(), &QAction::triggered, this, &QMainWindow::close);
    connect(mb->aSave(), &QAction::triggered, this, &Painter::saveImage);
}

void Painter::setPlaceholder()
{
    ws->lXYL()->pbDraw()->setToolTip("");

    if(ws->lXYL()->choiceFigure()->currentText() == "Circle")
    {
        ws->lXYL()->leX()->setPlaceholderText("X center");
        ws->lXYL()->leY()->setPlaceholderText("Y center");
        ws->lXYL()->leL()->setPlaceholderText("Diameter");
    }
    else if(ws->lXYL()->choiceFigure()->currentText() == "Triangle")
    {
        ws->lXYL()->leX()->setPlaceholderText("X top");
        ws->lXYL()->leY()->setPlaceholderText("Y top");
        ws->lXYL()->leL()->setPlaceholderText("Length base");
    }
    else if(ws->lXYL()->choiceFigure()->currentText() == "Square")
    {
        ws->lXYL()->leX()->setPlaceholderText("X left top");
        ws->lXYL()->leY()->setPlaceholderText("Y left top");
        ws->lXYL()->leL()->setPlaceholderText("Length");
    }
}

void Painter::x_addItemInList(PushButton* pb)
{
    QListWidgetItem* item = new QListWidgetItem(ws->objList()->lstWgt());
    item->setSizeHint(pb->sizeHint());
    item->setText(pb->text());
    ws->objList()->lstWgt()->setItemWidget(item, pb);
}

void Painter::drawFigure()
{
    if(ws->lXYL()->leX()->text().isEmpty() || ws->lXYL()->leY()->text().isEmpty() || ws->lXYL()->leL()->text().isEmpty())
        {
            QMessageBox::information(this, "Information", "Enter all values");
        }
        else if(ws->lXYL()->leX()->text().toInt() >= 500 || ws->lXYL()->leY()->text().toInt() >= 500)
        {
            QMessageBox::information(this, "Information", "Enter values X and Y less than 500");
        }
        else
        {
            int x = ws->lXYL()->leX()->text().toInt();
            int y = ws->lXYL()->leY()->text().toInt();
            int l = ws->lXYL()->leL()->text().toInt();
            std::vector<int> v {x, y, l};

            auto it = map_count.find(ws->lXYL()->choiceFigure()->currentText());
            if( it != map_count.end() )
                it->second++;
            else
            {
                map_count.emplace(ws->lXYL()->choiceFigure()->currentText(), 1);
                it = map_count.find(ws->lXYL()->choiceFigure()->currentText());
            }

            QString str = QString(it->first + QString::number(it->second));

            map_rect.emplace(str, v);

            auto it_figure = figure.find(ws->lXYL()->choiceFigure()->currentText());
            it_figure->second->drawFigure(v, ws->draw()->createPixmap(str));

            update();

            lst_pb.push_back(new PushButton(str));
            auto it_lst = lst_pb.end();
            --it_lst;
            x_addItemInList(*it_lst);
            connect(*it_lst, &QPushButton::toggled, this, &Painter::focusFigure);
        }
}

void Painter::focusFigure()
{
    QObject* sender = this->sender();
    PushButton* button = static_cast<PushButton*>(sender);
    QString str = button->text();
    QString str_cut = button->text();

    for(int i=0; i<str_cut.length(); i++)
        if (str_cut[i].isDigit())
            str_cut.remove(i, str_cut.length() - i);

    bool focus;
    button->isChecked() ? focus = true : focus = false;    

    auto it = map_rect.find(str);

    auto it_figure = figure.find(str_cut);
    it_figure->second->focusFigure(it->second, ws->draw()->getFocusPixmap(str), focus);

    update();
}

void Painter::deleteFigure()
{
    for(auto el=lst_pb.begin(); el!=lst_pb.end(); )
    {
        bool incr = true;
        if((*el)->isChecked())
        {
            (*el)->setChecked(false);
            QString str = (*el)->text();

            for (int i = 0; i < ws->objList()->lstWgt()->count(); i++)
            {
                if(ws->objList()->lstWgt()->item(i)->text() != (*el)->text())
                    continue;

                delete ws->objList()->lstWgt()->takeItem(i);

                ws->draw()->deletePixmap((*el)->text());

                auto it = map_rect.find(str);
                map_rect.erase(it);

                el = lst_pb.erase(el);
                incr = false;
            }
        }

        if ( incr )
            el++;
    }
}

void Painter::saveImage()
{
    QPixmap pix_save(500, 500);
    pix_save.fill();

    for(auto it_map_rect = map_rect.begin(); it_map_rect != map_rect.end(); it_map_rect++)
    {
        QString str = it_map_rect->first;
        for(int i=0; i<str.length(); i++)
            if(str[i].isDigit())
                str.remove(i, str.length() - i);

        auto it_figure = figure.find(str);
        it_figure->second->drawFigure(it_map_rect->second, pix_save);
    }

    QString strFilter;
    QString strName = QFileDialog::getSaveFileName(this, tr("Save Pixmap"), "../Doc/MyPixmap", "*.png ;; *.jpg ;; *.bmp", &strFilter);

    if(!strName.isEmpty())
    {
        if(strFilter.contains("jpg"))
            pix_save.save(strName + ".jpg", "JPG");
        else if(strFilter.contains("bmp"))
            pix_save.save(strName + ".bmp", "BMP");
        else pix_save.save(strName + ".png", "PNG");
    }
}

Painter::~Painter()
{
    //deleted by Qt
    //for(auto it = v.begin(); it != v.end(); it++)
    //    delete *it;
}
