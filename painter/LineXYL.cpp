#include "LineXYL.h"
#include <QRegExp>
#include <QValidator>
#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QComboBox>
#include <QPushButton>

LineXYL::LineXYL(QWidget* parent) : QWidget(parent)
{
    QRegExp rxp("^[0-9]\\d{1,2}$");
    QValidator* vld = new QRegExpValidator(rxp);

    QRegExp rxp_l("^[1-9]{1,1}[0-9]{2}$");
    QValidator* vld_l = new QRegExpValidator(rxp_l);

    QLabel* lbl_x = new QLabel("&X = ");
    QLabel* lbl_y = new QLabel("&Y = ");
    QLabel* lbl_l = new QLabel("&L = ");
    le_x = new QLineEdit;
    le_y = new QLineEdit;
    le_l = new QLineEdit;
    le_x->setValidator(vld);
    le_y->setValidator(vld);
    le_l->setValidator(vld_l);
    lbl_x->setBuddy(le_x);
    lbl_y->setBuddy(le_y);
    lbl_l->setBuddy(le_l);
    QHBoxLayout* lo_xyl = new QHBoxLayout;
    lo_xyl->addWidget(lbl_x);
    lo_xyl->addWidget(le_x);
    lo_xyl->addWidget(lbl_y);
    lo_xyl->addWidget(le_y);
    lo_xyl->addWidget(lbl_l);
    lo_xyl->addWidget(le_l);

    QLabel* lbl_figure = new QLabel("Figure:");

    cb_choiceFigure = new QComboBox;
    cb_choiceFigure->addItem(QPixmap("../Doc/Icon/circ.png"), "Circle");
    cb_choiceFigure->addItem(QPixmap("../Doc/Icon/tri.png"), "Triangle");
    cb_choiceFigure->addItem(QPixmap("../Doc/Icon/rec.png"), "Square");

    pb_draw = new QPushButton("Draw");

    QHBoxLayout* lo_figure = new QHBoxLayout;
    lo_figure->addWidget(lbl_figure);
    lo_figure->addWidget(cb_choiceFigure, 1);
    lo_figure->addWidget(pb_draw, 1);

    QVBoxLayout* lo_all = new QVBoxLayout;
    lo_all->addLayout(lo_figure);
    lo_all->addLayout(lo_xyl);

    setLayout(lo_all);
}
