#ifndef SQUARE_H
#define SQUARE_H

#include "Figure.h"

class Square : public Figure
{
public:
    Square();

    virtual void drawFigure(const std::vector<int>& v, QPixmap& pix) override;
    virtual void focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus) override;
};

#endif // SQUARE_H
