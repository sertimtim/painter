#include "Square.h"
#include <QPainter>

Square::Square()
{

}

void Square::drawFigure(const std::vector<int>& v, QPixmap& pix)
{
    int x = v[0];
    int y = v[1];
    int l = v[2];
    QRect square(x, y, l, l);

    QPainter paint;
    paint.begin(&pix);
    paint.setRenderHint(QPainter::Antialiasing, true);
    //paint.setBrush(QBrush(Qt::green, Qt::NoBrush));
    paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
    paint.drawRect(square);
    paint.end();
}

void Square::focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus)
{
    int x = v[0];
    int y = v[1];
    int l = v[2];
    QRect square(x, y, l, l);

    if(focus)
    {
        pix.fill(QColor(0, 0, 0, 0));
        QPainter paint;
        paint.begin(&pix);
        paint.setRenderHint(QPainter::Antialiasing, true);
        paint.setPen(QPen(Qt::blue, 6, Qt::SolidLine));
        paint.drawRect(square);
        paint.end();
    }
    else
    {
        pix.fill(QColor(0, 0, 0, 0));
        QPainter paint;
        paint.begin(&pix);
        paint.setRenderHint(QPainter::Antialiasing, true);
        paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
        paint.drawRect(square);
        paint.end();
    }
}
