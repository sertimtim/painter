#include "Triangle.h"
#include <QPainter>

Triangle::Triangle()
{

}

void Triangle::drawFigure(const std::vector<int>& v, QPixmap& pix)
{
    int x = v[0];
    int y = v[1];
    int l = v[2];
    QPolygon triangle;
    triangle << QPoint(x, y) << QPoint((x-l/2), (y+l)) << QPoint((x+l/2), (y+l));

    QPainter paint;
    paint.begin(&pix);
    paint.setRenderHint(QPainter::Antialiasing, true);
    //paint.setBrush(QBrush(Qt::green, Qt::NoBrush));
    paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
    paint.drawPolygon(triangle);
    paint.end();
}

void Triangle::focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus)
{
    int x = v[0];
    int y = v[1];
    int l = v[2];
    QPolygon triangle;
    triangle << QPoint(x, y) << QPoint((x-l/2), (y+l)) << QPoint((x+l/2), (y+l));

    if(focus)
    {
        QPolygonF triangle_scale = triangle;
        triangle_scale[0].setY(triangle_scale[0].y()*0.9);
        triangle_scale[1].setX(triangle_scale[1].x() - triangle_scale[0].x()*0.1);
        triangle_scale[1].setY(triangle_scale[1].y() + triangle_scale[0].y()*0.1);
        triangle_scale[2].setX(triangle_scale[2].x() + triangle_scale[0].x()*0.1);
        triangle_scale[2].setY(triangle_scale[2].y() + triangle_scale[0].y()*0.1);

        pix.fill(QColor(0, 0, 0, 0));
        QPainter paint;
        paint.begin(&pix);
        paint.setRenderHint(QPainter::Antialiasing, true);
        paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
        paint.drawPolygon(triangle_scale);
        paint.end();
    }
    else
    {
        pix.fill(QColor(0, 0, 0, 0));
        QPainter paint;
        paint.begin(&pix);
        paint.setRenderHint(QPainter::Antialiasing, true);
        paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
        paint.drawPolygon(triangle);
        paint.end();
    }
}
