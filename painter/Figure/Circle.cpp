#include "Circle.h"
#include <QPainter>

Circle::Circle()
{

}

void Circle::drawFigure(const std::vector<int>& v, QPixmap& pix)
{
    int x = v[0];
    int y = v[1];
    int l = v[2];
    QRect circle((x-l/2), (y-l/2), l, l);

    QPainter paint;
    paint.begin(&pix);
    paint.setRenderHint(QPainter::Antialiasing, true);
    //paint.setBrush(QBrush(Qt::green, Qt::NoBrush));
    paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
    paint.drawEllipse(circle);
    paint.end();
}

void Circle::focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus)
{
    int x = v[0];
    int y = v[1];
    int l = v[2];
    QRect circle((x-l/2), (y-l/2), l, l);

    if(focus)
    {
        pix.fill(QColor(0, 0, 0, 0));
        QPainter paint;
        paint.begin(&pix);
        paint.setRenderHint(QPainter::Antialiasing, true);
        paint.setPen(QPen(Qt::red, 3, Qt::SolidLine));
        paint.drawEllipse(circle);
        paint.end();
    }
    else
    {
        pix.fill(QColor(0, 0, 0, 0));
        QPainter paint;
        paint.begin(&pix);
        paint.setRenderHint(QPainter::Antialiasing, true);
        paint.setPen(QPen(Qt::blue, 3, Qt::SolidLine));
        paint.drawEllipse(circle);
        paint.end();
    }
}
