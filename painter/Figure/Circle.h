#ifndef CIRCLE_H
#define CIRCLE_H

#include "Figure.h"

class Circle : public Figure
{
public:
    Circle();

    virtual void drawFigure(const std::vector<int>& v, QPixmap& pix) override;
    virtual void focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus) override;
};

#endif // CIRCLE_H
