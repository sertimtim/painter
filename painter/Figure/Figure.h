#ifndef FIGURE_H
#define FIGURE_H

#include <QWidget>

class Figure
{
public:
    Figure()
    {

    }
    virtual void drawFigure(const std::vector<int>& v, QPixmap& pix) = 0;
    virtual void focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus) = 0;
    //QString getName() const { return name; }

    virtual ~Figure() {}

private:
    //QString name;
};

#endif // FIGURE_H
