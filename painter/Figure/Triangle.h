#ifndef TRIANGLE_H
#define TRIANGLE_H

#include "Figure.h"

class Triangle : public Figure
{
public:
    Triangle();

    virtual void drawFigure(const std::vector<int>& v, QPixmap& pix) override;
    virtual void focusFigure(const std::vector<int>& v, QPixmap& pix, bool focus) override;
};

#endif // TRIANGLE_H
