#ifndef MENUBAR_H
#define MENUBAR_H

#include <QWidget>

class QMenu;
class QToolBar;

class MenuBar : public QWidget
{
public:
    explicit MenuBar(QWidget *parent = nullptr);

    QMenu* mFile() const { return m_file; }
    QAction* aSave() const { return a_save; }
    QAction* aExit() const { return a_exit; }

private:
    QMenu* m_file;
    QAction* a_save;
    QAction* a_exit;
};

#endif // MENUBAR_H
