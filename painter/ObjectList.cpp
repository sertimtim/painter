#include "ObjectList.h"
#include <QLabel>
#include <QPushButton>
#include <QLayout>
#include <QListWidget>

ObjectList::ObjectList(QWidget *parent) : QWidget(parent)
{
    QLabel* lbl_list = new QLabel("Object list:");
    pb_delete = new QPushButton("Delete");

    lst_wgt = new QListWidget;    

    QVBoxLayout* lo_all = new QVBoxLayout;
    lo_all->addWidget(lbl_list);
    lo_all->addWidget(lst_wgt);
    lo_all->addWidget(pb_delete);

    setLayout(lo_all);
}
